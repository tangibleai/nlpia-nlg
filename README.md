# Natural Language Generation (NLG)

Natural language generation is the holy grail of AI research.

A bot that can chat can be tested for IQ.

But to really test it you want to release it in a web app...

 
 
---




# Gradio

```python
import gradio

description = "Natural language generation with GPT-2"
title = "Give GPT-2 a prompt and it will take it from there..."
examples = [["What does sand between your toes feel like?"]]
model_path = "ethzanalytics/distilgpt2-tiny-conversational"
# model_path = "huggingface/pranavpsv/gpt2-genre-story-generator"
```

  
 
---




# Web interface

```python
interface = gr.Interface.load(
    model_path,
    description=description,
    examples=examples,
)
interface.launch()
```


---
