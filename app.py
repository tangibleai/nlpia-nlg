# https://huggingface.co/blog/gradio-spaces

import gradio

description = "Natural language generation with GPT-2"
title = "Give GPT-2 a prompt and it will take it from there..."
examples = [["What does sand between your toes feel like?"]]
model_path = "huggingface/CodeBERTa-small-v1"
tokenizer_path = "huggingface/CodeBERTa-small-v1"
model_path = "huggingface/pranavpsv/gpt2-genre-story-generator"

interface = gradio.Interface.load(
    model_path,
    description=description,
    examples=examples,
)
interface.launch()
