# code_app.py

from transformers import pipeline

fill_mask = pipeline(
    "fill-mask",
    model="huggingface/CodeBERTa-small-v1",
    tokenizer="huggingface/CodeBERTa-small-v1"
)

text = """
def pipeline(
    task: str,
    model: Optional = None,
    framework: Optional[<mask>] = None,
    **kwargs
) -> Pipeline:
    pass
""".lstrip()

fill_mask(text)


import gradio as gr
from transformers import pipeline

pipeline = pipeline(
    # task="text-generation",
    model="huggingface/CodeBERTa-small-v1",
    tokenizer="huggingface/CodeBERTa-small-v1")


def predict(text):
    predictions = pipeline(image)
    return predictions


gradio.Interface(
    fill_mask,
    inputs=inputs=gradio.Textbox(value=text),
    outputs=gradio.outputs.Textbox(),
    title="Code completion",
).launch()
